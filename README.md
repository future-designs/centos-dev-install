# Usage
Install:
```bash
bash centos-dev-install.sh
```
## manual installation
### epel and webtatic
```bash
sudo yum -y update
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo yum -y update
```
### php
```bash
sudo yum -y install mod_php71w php71w-opcache php71w-cli php71w-common php71w-gd php71w-intl php71w-mbstring php71w-mcrypt php71w-mysql php71w-mssql php71w-pdo php71w-pear php71w-soap php71w-xml php71w-xmlrpc
```
### docker
```bash
sudo yum -y install docker
```
### composer
```bash
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```
### docker-compose
https://github.com/docker/compose/releases
-> look for newest version
```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```
### git
```bash
sudo yum -y install git
```
### ssh

```bash
sudo ssh-keygen -t rsa
```
cat ~/.ssh/id_rsa.pub

### mariadb
```bash
sudo yum install mariadb-server mariadb-client
```
Autostart and first start:
```
sudo systemctl enable mariadb
sudo systemctl start mariadb
```
Login with:
```
sudo mysql -u root -p
```
(default no pass)
#### new database and user with rights
Login into mariadb...
```mysql
CREATE DATABASE your_db_name.db;
CREATE USER 'your-user'@'%' IDENTIFIED BY 'your-new-pass';
GRANT ALL ON your_db_name.db.* TO 'your-user'@'%';
```
To allow no remote access for the user write 'user'@'localhost' instead of 'user'@'%'